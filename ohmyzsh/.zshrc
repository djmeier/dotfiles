# Path
if [[ ! -d ${HOME}/.bin ]]; then
  mkdir ${HOME}/.bin
fi

export PATH="${HOME}/.bin:${PATH}"

# Check if oh-my-zsh is installed.
if [[ ! -d ${HOME}/.oh-my-zsh ]]; then
  git clone https://github.com/robbyrussell/oh-my-zsh.git ${HOME}/.oh-my-zsh
  chsh -s /bin/zsh
fi

# Check if fzf-tab is installed.
if [[ ! -d ${HOME}/.oh-my-zsh/custom/plugins/fzf-tab ]]; then
  git clone https://github.com/Aloxaf/fzf-tab ${HOME}/.oh-my-zsh/custom/plugins/fzf-tab
fi

# Path to your oh-my-zsh installation.
export ZSH="${HOME}/.oh-my-zsh"

# General config
ZSH_THEME="robbyrussell"
export UPDATE_ZSH_DAYS=5
zstyle ':omz:update' frequency 5

# OOtB plugins only.
plugins=(
  common-aliases
  colored-man-pages
  colorize
  command-not-found
  copyfile
  copypath
  cp
  dircycle
  docker
  docker-compose
  docker-machine
  encode64
  extract
  fancy-ctrl-z
  fzf
  fzf-tab
  git
  gpg-agent
  history
  kubectl
  minikube
  pip
  pyenv
  python
  sudo
  systemd
  terraform
  web-search
  z
)

# Linux specific configurations.
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  if [[ ! -d ${HOME}/.linuxbrew ]] && [[ ! -d /home/linuxbrew/.linuxbrew ]]; then
    export IS_HOMEBREW=False
    echo "### Homebrew not installed."
  else
    export IS_HOMEBREW=True
    plugins+=(brew)
    if [[ -d ${HOME}/.linuxbrew ]]; then
      eval $(${HOME}/.linuxbrew/bin/brew shellenv)
    else
      eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
    fi
  fi
  if [[ ! -d ${HOME}/Apps ]]; then
    mkdir ${HOME}/Apps
  fi
  if [[ -d ${HOME}/.cargo/bin ]]; then
    export PATH="${HOME}/.cargo/bin:${PATH}"
  fi 
  if [[ -d /usr/local/go/bin ]]; then
    export PATH="/usr/local/go/bin:${PATH}"
  fi
  export PATH="${HOME}/Apps:${PATH}"
fi

# Mac OSX specific configurations.
if [[ "$OSTYPE" == "darwin"* ]]; then
  if type brew &>/dev/null
  then
    FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"
    export IS_HOMEBREW=True
    plugins+=(brew)
  fi
fi

# Check for fzf and enable plugins if exists.
if ! [[ -x "$(command -v fzf)" ]]; then
  export IS_FZF=False
  echo "### Binary for fzf not installed."
else
  export IS_FZF=True
  plugins+=(fzf fzf-tab)
  [[ -f ${HOME}/.fzf.zsh ]] && source ${HOME}/.fzf.zsh
fi

# Check for Docker and enable or configure specific tooling.
if ! [[ -x "$(command -v docker)" ]]; then
  export IS_DOCKER=False
  echo "### Docker isn't installed, certain functionality unavailable."
else
  export IS_AWS=True
  plugins+=(aws)
fi

# Pyenv
export PIPENV_VENV_IN_PROJECT=1
export PYENV_ROOT="${HOME}/.pyenv"
export PATH="${PYENV_ROOT}/bin:${PATH}"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init --path)"
  eval "$(pyenv virtualenv-init -)"
fi

# Activate Oh-My-ZSH
autoload bashcompinit && bashcompinit
autoload -Uz compinit && compinit
source ${ZSH}/oh-my-zsh.sh

# Fix to enable AWS CLI v2 completion.
if [[ IS_AWS==True ]]; then
  complete -C '/usr/local/bin/aws_completer' aws
fi

# Post ZSH activation OS specific configuration
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  # Linux
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Mac OSX
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
elif [[ "$OSTYPE" == "msys" ]]; then
  # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
elif [[ "$OSTYPE" == "win32" ]]; then
  # Hopefully we never run this segment
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  # Probably not
else
  # Probably fail
fi

# Setup broot
test -e "${HOME}/.config/broot/launcher/bash/br" && source ${HOME}/.config/broot/launcher/bash/br

# Aliases, because: lazy
alias wormhole='docker run -it --tty --rm -v `pwd`:`pwd` -w `pwd` splatops/magic-wormhole:latest'
alias git_force_reset='git fetch origin && git reset --hard origin/master && git clean -f -d'
