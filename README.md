# Dotfiles README
Some quick instructions for installing all the dotfiles. Nothing special here.

## Install:
```
[apt||brew] install fzf git tmux vim zsh
cd ~
git clone https://gitlab.com/djmeier/dotfiles.git .dotfiles
ln -s ~/.dotfiles/vim ~/.vim
ln -s ~/.dotfiles/vim/vimrc ~/.vimrc
ln -s ~/.dotfiles/tmux ~/.tmux
ln -s ~/.dotfiles/tmux/tmux.conf ~/.tmux.conf
ln -s ~/.dotfiles/ohmyzsh/.zshrc ~/.zshrc
sudo ln -s ~/.dotfiles/aws/aws /usr/local/bin/
chmod +x ~/.dotfiles/aws/aws
sudo ln -s ~/.dotfiles/aws/aws_completer /usr/local/bin/
chmod +x ~/.dotfiles/aws/aws_completer
```

## Update tmux:

NOTE:
When adding git repositories as plugins use submodule.
```
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
tmux && tmux source ~/.tmux.conf
[prefix] + I
```

## Adding plugins for tmux:
```
git submodule add https://github.com/repo/repo.git bundle/repo
```
TPM GitHub link: [tmux-plugins/tpm](https://github.com/tmux-plugins/tpm "github.com/tmux-plugins/tpm")

## Update vim:

NOTE:
Using git submodules to manage vim bundles and those need to be updated.
```
cd ~/.vim
git submodule init
git submodule update
```

So you wanna upgrade all bundle modules?
```
cd ~/.vim/bundle
git submodule foreach git pull origin master
```
Synchronizing plugins with git submodules and pathogen link: [vimcasts](http://vimcasts.org/episodes/synchronizing-plugins-with-git-submodules-and-pathogen/ "vimcasts")


